#What is Wallie?
_Wallie_ is an open source project that provide a simple way to integrate [Spine](http://esotericsoftware.com/)'s animation as a Live Wallpaper on Android.

#What's the licence?
It depends on the parts of the project :

AndroidSDK/AndroidNDK are free to use as long as you accept their [Terms and Conditions](http://developer.android.com/sdk/terms.html).
[cocos2dx](http://www.cocos2d-x.org/) is free to use and released under MIT Licence.

[spine-cocos2dx runtime](https://github.com/EsotericSoftware/spine-runtimes/tree/master/spine-cocos2dx) is released under the Spine Runtime Licence v2. Basically you need a Spine licence to use it.

The art assets and resources are the property of their respective authors.

#How does it works?
Wallie uses the AndroidSDK to build a _WallpaperService.Engine_ responsible for rendering a live wallpaper.

The Live wallpaper itself uses [cocos2dx](http://cocos2dx.org/) for rendering and processing inputs. The animation uses [Spine](http://esotericsoftware.com/) runtime and exports to be updated/displayed.

#I own a Spine licence, how do I use it?
The program will parse your animation export. You just have to put your skeleton.json and atlas files into /Resources.

The following rules applies :

+ Animations containing the word "idle" will be played randomly
+ Animation containing the word "important" will never be interrupted by an other animation
+ The default animation for the character is named "idle"
+ Some animations are played on particular events :
    + onSwipe - triggered with a lateral swipe. The character will be fliped X wide depending on the direction of the gesture
    + onSwipeUp
    + onTap
    + onTapOut - when the tap is out of the character
    + onAccelerate - is triggered when the device is moved lateraly (x axis)
		
