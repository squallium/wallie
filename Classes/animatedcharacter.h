#ifndef ANIMATEDCHARACTER_H
#define ANIMATEDCHARACTER_H

#include "cocos2d.h"
#include "spine/spine-cocos2dx.h"
#include "spine/CCSkeletonAnimation.h"
#include "string.h"

class AnimatedCharacter : public spine::CCSkeletonAnimation
{
public:
    AnimatedCharacter (const char* skeletonDataFile, const char* atlasFile, float scale = 0);
    static AnimatedCharacter* createWithFile(const char *skeletonDataFile, const char *atlasFile, float scale);

    /**
     * @brief onSwipe called on Swipe action, triggers swipe behavior (lateralJump() for example)
     * @param pDirection direction of the swipe gesture
     */
    void onSwipe(cocos2d::CCPoint pDirection);


    /**
     * @brief onTap triggers tap behavior (looking at the tap position for example)
     * @param pPosition position of the tap on the screen
     */
    void onTap(cocos2d::CCPoint pPosition);


    /**
     * @brief onAccelerate handles acceleration read by the accelerometer of the device
     * @param pAcceleration CCAcceleration object holding the x,y,z values for the accelerations
     */
    void onAccelerate(cocos2d::CCAcceleration* pAcceleration);


    /**
     * @brief setIdleAnimation sets the default animation to loop between event animations
     * @param pIdleAnimationName name of the animation (default is "idle")
     */
    void setIdleAnimation(std::string pIdleAnimationName);


    /**
     * @brief m_maximumDefaultIdleAnimation maximum number of times the default animation loops before
     * triggering an other idle animation
     */
    int m_maximumDefaultIdleAnimation;

    virtual void update(float deltaTime);

private:
    /**
     * @brief m_idleAnimationName default animation to loop on
     */
    std::string m_idleAnimationName;


    /**
     * @brief m_idleAnimationList list of idle animation to use when being idle
     */
    std::vector<const char*> m_idleAnimationList;


    /**
     * @brief m_unInterruptibleAnimationList list of animations that can't be interrupted
     */
    std::vector<const char*> m_unInterruptibleAnimationList;


    /**
     * @brief initAnimations initializes all animations and stores them in the correct lists
     */
    void initAnimations();


    /**
     * @brief setDefaultMix sets the default time to mix from an animation to an other (common
     * to all animations on the character)
     * @param pTime duration of the mix in seconds (default 0.2f)
     */
    void setDefaultMix(float pTime);

    /**
     * @brief setNewIdleAnimation sets a new animation when the default one as looped more than m_maximumDefaultIdleAnimation
     */
    void setNewIdleAnimation();


    /**
     * @brief isInterruptible tells if the current animation should be interrupted or not
     * @return true if it can be interrupted
     */
    bool isInterruptible();

    /**
     * @brief onAnimationStateEvent callback function called on every animation event of the character (begin, end, complete...)
     * @param trackIndex the track index of the animation calling
     * @param type type of event (ANIMATION_END, ANIMATION_START, ANIMATION_COMPLETE, ANIMATION_EVENT)
     * @param event the custom event object
     * @param loopCount the number of times the current animation as already looped
     */
    void onAnimationStateEvent(int trackIndex, spEventType type, spEvent *event, int loopCount);

    /**
     * @brief lateralJump makes the character jump on a side of the screen on reaction to a lateral swipe
     * @param pDirection direction of the jump
     */
    void lateralJump(cocos2d::CCPoint pDirection);


    /**
     * @brief jump runs a jump animation (on swipe up for example)
     */
    void jump();

    bool lookAt(cocos2d::CCPoint pPosition);
};

#endif // ANIMATEDCHARACTER_H
