#ifndef _WALLLAYER_H_
#define _WALLLAYER_H_

#include "cocos2d.h"
#include "animatedcharacter.h"

class WallLayer: public cocos2d::CCLayer {
public:
	static cocos2d::CCScene* scene ();

	virtual bool init ();
	virtual void update (float deltaTime);
	virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
	virtual void didAccelerate(cocos2d::CCAcceleration* pAcceleration);

    /**
     * @brief createNewCloud callback function used by clouds when they are out of screen so a new one can be created
     */
	void createNewCloud();

	CREATE_FUNC (WallLayer);


private:
    /**
     * @brief cloudImageList lost of image usable for clouds passing by
     */
	std::vector <char*> cloudImageList;

    /**
     * @brief cloudAmount maximum amount of cloud
     */
	int cloudAmount;

    /**
     * @brief swipeThreshold distance over which we consider a touch to be a swipe
     */
	int swipeThreshold;

    /**
     * @brief accelerationThreshold force over which we consider that the character should have a reaction (may vary on different devices)
     */
	int accelerationThreshold;

    /**
     * @brief mainCharacter main character populating the screen
     */
    AnimatedCharacter* mainCharacter;


    /**
     * @brief initClouds inits the clouds
     */
	void initClouds();
	void applyPreferences();

};

#endif // _WALLLAYER_H_
